import java.util.*;

/**
 * I'm using ideas from following sources:
 * http://algs4.cs.princeton.edu/55compression/Huffman.java
 * https://en.wikipedia.org/wiki/Huffman_coding
 * http://www.cs.armstrong.edu/liang/intro9e/html/HuffmanCode.html
 * https://www.reddit.com/r/javaexamples/comments/3gatvr/intermediate_huffman_tree_encoding/
 * http://web.cs.wpi.edu/~cs2223/d09/HW/HW4/Solutions/Code/Huffman.java
 * https://www.youtube.com/watch?v=ceECSn0W3pE
 * https://rosettacode.org/wiki/Huffman_coding#Java
 * http://codereview.stackexchange.com/questions/44473/huffman-code-implementation
 * http://www.geeksforgeeks.org/greedy-algorithms-set-3-huffman-coding/
 */
/**
 * Prefix codes and Huffman tree. Tree depends on source data.
 */
public class Huffman {

	/** I will assume: all bytes have code less than 256. */
	private int[] frequencies;

	/** Table */
	private Map<Byte, Leaf> table;

	/** Bits */
	private int length;
	

	/**
	 * Constructor to build the Huffman code for a given bytearray.
	 * 
	 * @param original
	 *            source data
	 */
	Huffman(byte[] original) {
		frequencies = new int[256];
		table = new HashMap<Byte, Leaf>();
		length = 0;
		init(original);

	}

	/**
	 * Length of encoded data in bits.
	 * 
	 * @return number of bits
	 */
	public int bitLength() {
		return length;
	}

	/**
	 * Encoding the byte array using this prefixcode.
	 * 
	 * @param origData
	 *            original data
	 * @return encoded data
	 */
	public byte[] encode(byte[] origData) {
		StringBuffer temp = new StringBuffer();
		for (byte b : origData)
			temp.append(table.get(b).code);
		length = temp.length();
		List<Byte> bytes = new ArrayList<Byte>();
		while (temp.length() > 0) {
			while (temp.length() < 8)
				temp.append('0');
			String stri = temp.substring(0, 8);
			bytes.add((byte) Integer.parseInt(stri, 2));
			temp.delete(0, 8);
		}
		byte[] rtn = new byte[bytes.size()];
		for (int i = 0; i < bytes.size(); i++)
			rtn[i] = bytes.get(i);

		return rtn;
	}

	/**
	 * Decoding the byte array using this prefixcode.
	 * 
	 * @param encodedData
	 *            encoded data
	 * @return decoded data (hopefully identical to original)
	 */
	public byte[] decode(byte[] encodedData) {
		StringBuffer temp = new StringBuffer();
		for (int i = 0; i < encodedData.length; i++)
			temp.append(String.format("%8s", Integer.toBinaryString(encodedData[i] & 0xFF)).replace(' ', '0'));
		String str = temp.substring(0, length);
		List<Byte> bytes = new ArrayList<Byte>();
		String code = "";
		while (str.length() > 0) {
			code += str.substring(0, 1);
			str = str.substring(1);
			Iterator<Leaf> list = table.values().iterator();
			while (list.hasNext()) {
				Leaf leaf = list.next();
				if (leaf.code.equals(code)) {
					bytes.add(leaf.value);
					code = "";
					break;
				}
			}
		}

		byte[] rtn = new byte[bytes.size()];
		for (int i = 0; i < bytes.size(); i++)
			rtn[i] = bytes.get(i);
		return rtn;
	}

	/** Main method. */
	public static void main(String[] params) {
		String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
		//String tekst ="eebbeecdebeeebecceeeddebbbeceedebeeddeeeecceeeedeeedeeebeedeceedebeeedeceeedebee";
		//String tekst = "BACADAEAFABBAAAGAH"; 
		byte[] orig = tekst.getBytes();
		Huffman huf = new Huffman(orig);
		byte[] kood = huf.encode(orig);
		byte[] orig2 = huf.decode(kood);
		// must be equal: orig, orig2
		System.out.println(Arrays.equals(orig, orig2));
		int lngth = huf.bitLength();
		System.out.println("Length of encoded data in bits: " + lngth);
		// TODO!!! Your tests here!
	}
	

	private void init(byte[] data) {
		for (byte b : data)
			frequencies[b]++;
		PriorityQueue<Tree> trees = new PriorityQueue<Tree>();
		for (int i = 0; i < frequencies.length; i++)
			if (frequencies[i] > 0)
				trees.offer(new Leaf(frequencies[i], (byte) i));
		assert trees.size() > 0;
		while (trees.size() > 1)
			trees.offer(new Node(trees.poll(), trees.poll()));
		Tree tree = trees.poll();
		code(tree, new StringBuffer());
	}

	private void code(Tree tree, StringBuffer prefix) {
		assert tree != null;
		if (tree instanceof Leaf) {
			Leaf leaf = (Leaf) tree;
			leaf.code = (prefix.length() > 0) ? prefix.toString() : "0";
			table.put(leaf.value, leaf);
		} else if (tree instanceof Node) {
			Node node = (Node) tree;
			prefix.append('0');
			code(node.left, prefix);
			prefix.deleteCharAt(prefix.length() - 1);
			prefix.append('1');
			code(node.right, prefix);
			prefix.deleteCharAt(prefix.length() - 1);
		}
	}

	/** Huffman tree. */
	abstract class Tree implements Comparable<Tree> {

		/** The tree frequency */
		public final int frequency;

		public Tree(int frequency) {
			this.frequency = frequency;
		}

		@Override
		public int compareTo(Tree o) {
			return frequency - o.frequency;
		}

	}

	/** Huffman leaf */
	class Leaf extends Tree {

		public String code;
		public final byte value;

		public Leaf(int frequency, byte value) {
			super(frequency);

			this.value = value;
		}

	}

	/** Huffman node. */
	class Node extends Tree {

		public final Tree left, right;

		public Node(Tree left, Tree right) {
			super(left.frequency + right.frequency);
			this.left = left;
			this.right = right;
		}

	}
}